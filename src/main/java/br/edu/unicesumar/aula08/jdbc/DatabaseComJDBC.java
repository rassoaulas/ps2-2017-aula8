package br.edu.unicesumar.aula08.jdbc;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class DatabaseComJDBC {

    private static final String CREATE = "" +
            "CREATE TABLE " +
            "public.teste ( " +
            "id NUMERIC PRIMARY KEY NOT NULL, " +
            "descricao VARCHAR );";

    private static final String DROP = "" +
            "DROP TABLE public.teste;";

    private static final String INSERT = "" +
            "INSERT INTO public.teste " +
            "(id, descricao) " +
            "VALUES (3, 'descricao 3');";

    private static final String DELETE =
            "DELETE FROM public.teste " +
                    "WHERE id = 2;";

    private static final String SELECT =
            "SELECT * FROM public.teste;";

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres?user=postgres&password=123456";

    private static Connection connection;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        connection = DriverManager.getConnection(DB_URL);
        selecionarRegistros();
        connection.close();
    }

    private static void executar(String comando) throws SQLException {
        Statement statement = connection
                .createStatement();
        statement.execute(comando);
    }

    private static List<Teste> selecionarRegistros() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT);
        List<Teste> resultado = new LinkedList<>();
        while (rs.next()) {
            Integer id = rs.getInt("id");
            String descricao = rs.getString("descricao");
            resultado.add(new Teste(id, descricao));
        }
        return resultado;
    }

}
