package br.edu.unicesumar.aula08.jdbc;

public class Teste {

    private final Integer id;
    private final String descricao;

    public Teste(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return id + " - " + descricao;
    }
}
