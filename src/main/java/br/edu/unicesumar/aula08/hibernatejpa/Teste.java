package br.edu.unicesumar.aula08.hibernatejpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Teste implements Serializable {

	private static final long serialVersionUID = 7418355723871056649L;

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String descricao;

	@Column
	private String descricaoCurta;

	Teste(){
	}

	private Teste(String descricao){
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return id + " - " + descricao;
	}

}
