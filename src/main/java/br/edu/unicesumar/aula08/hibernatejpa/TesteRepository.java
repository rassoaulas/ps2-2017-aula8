package br.edu.unicesumar.aula08.hibernatejpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TesteRepository extends JpaRepository<Teste, Long> {

}
