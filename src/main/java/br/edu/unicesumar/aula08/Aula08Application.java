package br.edu.unicesumar.aula08;

import br.edu.unicesumar.aula08.hibernatejpa.Teste;
import br.edu.unicesumar.aula08.hibernatejpa.TesteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Aula08Application {

	private static TesteRepository testeRepository;

	@Autowired
	public Aula08Application(TesteRepository testeRepository) {
		this.testeRepository = testeRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(Aula08Application.class, args);
		System.out.println(testeRepository.findAll());
	}
}
